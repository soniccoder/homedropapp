package com.example.harshkhare.homedropapp;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.bloder.magic.view.MagicButton;

import static com.google.common.reflect.Reflection.initialize;



public class MainActivity extends AppCompatActivity {
    String number="8655891459";
    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;
    private static final String[] REQUIRED_SDK_PERMISSIONS = new String[] {
            Manifest.permission.INTERNET,
            Manifest.permission.CALL_PHONE,
            Manifest.permission.SEND_SMS,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE
    };
    int requestCode;
    @NonNull String permissions[];
    @NonNull int[] grantResults;
    private MagicButton button1, button2, button3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        checkPermissions();
        onRequestPermissionsResult(requestCode,permissions,grantResults);
        opencontent();
    }
@Override public void onBackPressed(){
    AlertDialog.Builder builder=new AlertDialog.Builder(MainActivity.this);
    builder.setMessage("Do you want to Exit Home Drop App?")
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.super.onBackPressed();
                }
            })
            .setNegativeButton("No",null).setCancelable(false);
    AlertDialog alert=builder.create();
    alert.show();
}
   private void opencontent(){
    button1 = (MagicButton) findViewById(R.id.adminLogin);
    //  mRoot=findViewById(R.id.mRoot);
    button1.setMagicButtonClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openAdmilLogin();
        }
    });

    button2 = (MagicButton) findViewById(R.id.XYZMaps);

    button2.setMagicButtonClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            openXYZMaps();
        }
    });

    button3= (MagicButton) findViewById(R.id.sos);
    button3.setMagicButtonClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            android.app.AlertDialog.Builder builder =new android.app.AlertDialog.Builder(MainActivity.this);
            builder.setMessage("Do you want to send SOS message and call?")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            makePhoneCall();
                            sendSMS();
                        }
                    }).setNegativeButton("Cancel",null);
            android.app.AlertDialog alert=builder.create();
            alert.show();
        }
    });

}
    private void openAdmilLogin() {
        Intent intent = new Intent(this, Login.class);

        startActivity(intent);

    }

    private void openXYZMaps() {
        Intent intent = new Intent(this, com.example.harshkhare.homedropapp.IDOLocations.class);

        startActivity(intent);

    }
    protected void checkPermissions(){
        final List<String> missingPermissions = new ArrayList<String>();
        // check all required dynamic permissions
        for (final String permission : REQUIRED_SDK_PERMISSIONS) {
            final int result = ContextCompat.checkSelfPermission(this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission);
            }
        }
        if (!missingPermissions.isEmpty()) {
            // request all missing permissions
            final String[] permissions = missingPermissions
                    .toArray(new String[missingPermissions.size()]);
            ActivityCompat.requestPermissions(this, permissions, REQUEST_CODE_ASK_PERMISSIONS);
        } else {
            final int[] grantResults = new int[REQUIRED_SDK_PERMISSIONS.length];
            Arrays.fill(grantResults, PackageManager.PERMISSION_GRANTED);
            onRequestPermissionsResult(REQUEST_CODE_ASK_PERMISSIONS, REQUIRED_SDK_PERMISSIONS,
                    grantResults);
        }
    }
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                for (int index = permissions.length - 1; index >= 0; --index) {
                    if (grantResults[index] != PackageManager.PERMISSION_GRANTED) {
                        // exit the app if one permission is not granted
                        Toast.makeText(this, "Required permission '" + permissions[index]
                                + "' not granted, exiting", Toast.LENGTH_LONG).show();
                        finish();
                        return;
                    }
                }
                initialize();
                break;
        }
    }
    private void makePhoneCall() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(MainActivity.this, "Please grant call permissions!", Toast.LENGTH_SHORT).show();
        } else {
            String dial = "tel:" + number;
            startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(dial)));
        }
    }

    private void sendSMS() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(MainActivity.this, "Please grant sms permissions!", Toast.LENGTH_SHORT).show();
        } else {
            String msg = "Please help I am stuck in an emergency situation!";
            String dial = "tel:" + number;
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(dial, null, msg, null, null);
            Toast.makeText(MainActivity.this, "Message sent successfully!", Toast.LENGTH_SHORT).show();
        }
    }


}

