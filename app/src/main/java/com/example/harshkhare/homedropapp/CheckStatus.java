package com.example.harshkhare.homedropapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.util.Calendar;

public class CheckStatus extends AppCompatActivity {


    public static final String Name = "Employee Name ";
    public static final String Cab_Num = "Cab Number ";
    public static final String Cab_time = "Cab Time ";
    public static final String Cab_Comm = "Cab Comments";
    public static final String reached = " Drop Status";
    public static final String TAG = " Hello ";


    Calendar c = Calendar.getInstance();
    String CurrentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

    DocumentReference Docref = FirebaseFirestore.getInstance().collection("DropData").document(CurrentDate).collection("Nitesh Tiwari").document("Details");

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_status);

    }

    public void GetData(View view) {

        final TextView NAME = findViewById(R.id.name);
        final TextView CAB = findViewById(R.id.cabnum);
        final TextView TIME = findViewById(R.id.timestatus);
        final TextView COMMENT = findViewById(R.id.commentstatus);
        final TextView STATUS = findViewById(R.id.status);

        Docref.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {

                    String NAMES = documentSnapshot.getString(Name);
                    String Cab = documentSnapshot.getString(Cab_Num);
                    String Time = documentSnapshot.getString(Cab_time);
                    String Comments = documentSnapshot.getString(Cab_Comm);
                    String Status = documentSnapshot.getString(reached);

                    NAME.setText(NAMES);
                    CAB.setText(Cab);
                    TIME.setText(Time);
                    COMMENT.setText(Comments);
                    STATUS.setText(Status);


                }

            }
        });

    }
}
