package com.example.harshkhare.homedropapp;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.harshkhare.homedropapp.R;

public class UserPage extends AppCompatActivity {

    private Button button1, button2, feedback,mapfetch;
    private ImageView sos, sosemail;
    private String number = "+918655891459";
    private TextView gpsshow;
    private LocationManager locationManager;
    private LocationListener locationListener;
    String gpsdata;


    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_page);
        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openroster();
            }


        });

        feedback = (Button) findViewById(R.id.button3);
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openRideFeedback();
            }
        });

        sosemail = (ImageView) findViewById(R.id.sosemail);
        sosemail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder =new AlertDialog.Builder(UserPage.this);
                builder.setMessage("Do you want to send SOS via E-Mail?")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendEmail();
                            }
                        }).setNegativeButton("Cancel",null);
                AlertDialog alert=builder.create();
                alert.show();
            }
        });

        mapfetch=findViewById(R.id.heremapsbtn);
        mapfetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(UserPage.this,Here.class);
                startActivity(intent);
            }
        });
    }


    private void openRideFeedback() {
        Intent intent = new Intent(this, com.example.harshkhare.homedropapp.RideFeedback.class);

        startActivity(intent);

    }

    private void openroster() {

        Intent intent = new Intent(this, com.example.harshkhare.homedropapp.Roster.class);

        startActivity(intent);
    }

    private void sendEmail() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setData(Uri.parse("mail.to:"));
        String[] recipients = {"harsh.khare@here.com", "nitesh.tiwari@here.com", "abhishek.vaidya@here.com"};
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Emergency! Need help!");
        intent.putExtra(Intent.EXTRA_TEXT, "Please contact me on my number, It's an emergency!  This message was sent by HomeDropApp , @Here.com");
        intent.setType("message/rfc822");
        Intent chooser = Intent.createChooser(intent, "Send Email");
        startActivity(chooser);
    }

    private void gpsdatamethod() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                gpsshow.append("\n Latitude: "+location.getLatitude()+"\n Longitude: "+location.getLongitude());
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Toast.makeText(UserPage.this,"Please provide GPS permission", Toast.LENGTH_SHORT).show();
            }
        };
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(UserPage.this,"Please provide GPS permission", Toast.LENGTH_SHORT).show();
        }
        else {
            locationManager.requestLocationUpdates("gps", 0000, 0, locationListener);
        };
    }
}

