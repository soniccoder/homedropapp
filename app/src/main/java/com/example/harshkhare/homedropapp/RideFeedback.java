package com.example.harshkhare.homedropapp;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class RideFeedback extends AppCompatActivity {

    public static final String Name = "Employee Name ";
    public static final String Cab_Num = "Cab Number ";
    public static final String Cab_time = "Cab Time ";
    public static final String Cab_Comm = "Cab Comments";
    public static final String reached = " Drop Status";
    public static final String TAG = " Hello ";


    Calendar c = Calendar.getInstance();
    String CurrentDate = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

    String Names;
    String Cab;
    String Comm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_feedback);

        final TextView Date = findViewById(R.id.date);
        Date.setText(CurrentDate);

        Switch aSwitch = findViewById(R.id.switch1);
        final TextView text = findViewById(R.id.textView4);

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {



                    TextView Timeo = findViewById(R.id.timer);
                    EditText nam = findViewById(R.id.name);
                    EditText Cabno = findViewById(R.id.cabno);
                    EditText Comment = findViewById(R.id.comment);

                    Names = nam.getText().toString();

                    Cab = Cabno.getText().toString();
                    Comm = Comment.getText().toString();
                    String cab_time = Timeo.getText().toString();
                    text.setText("Reached");
                    String reac = text.getText().toString();

                    Timeo.setText(c.get(Calendar.HOUR) + ":" + c.get(Calendar.MINUTE) + ":" + c.get(Calendar.SECOND));

                    if(Names.isEmpty() || Cab.isEmpty() || Comm.isEmpty())
                    {

                        Toast.makeText(getApplicationContext(), "Kindly fill in the details..!!", Toast.LENGTH_LONG).show();
                        return;
                    }
                    else
                    {


                    Map<String, Object> datatosave = new HashMap<String, Object>();
                    datatosave.put(Cab_Num, Cab);
                    datatosave.put(Cab_time, cab_time);
                    datatosave.put(Cab_Comm, Comm);
                    datatosave.put(reached, reac);
                    datatosave.put(Name, Names);

                    DocumentReference Docref = FirebaseFirestore.getInstance().collection("DropData").document(CurrentDate).collection(Names).document("Details");

                    Toast.makeText(getApplicationContext(), "Your Feedback Has Been Recorded", Toast.LENGTH_LONG).show();


                    Docref.set(datatosave).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                    @Override
                                                                    public void onSuccess(Void aVoid) {
                                                                        Log.d(TAG, "Document has been saved!");
                                                                    }
                                                                }
                    ).addOnFailureListener(new OnFailureListener() {
                                               @Override
                                               public void onFailure(@NonNull Exception e) {
                                                   Log.w(TAG, "Document was not saved!", e);
                                               }

                                           }

                    );
                }
                }
                else
                    {
                    text.setText("Not Reached");
                    String reac = text.getText().toString();

                    if (reac.isEmpty()) {
                        return;
                    }
                    DocumentReference Docref = FirebaseFirestore.getInstance().collection("DropData").document(CurrentDate).collection(Names).document("Details");

                    Map<String, Object> datatosave = new HashMap<String, Object>();

                    datatosave.put(reached, reac);

                    Toast.makeText(getApplicationContext(), "Kindly Switch The Button When Reached Home", Toast.LENGTH_LONG).show();

                    Docref.set(datatosave).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Log.d(TAG, "Document has been saved!");
                        }

                    }).addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.w(TAG, "Document was not saved!", e);
                                                }

                                            }
                    );


                }
            }


        });
    }
}
