package com.example.harshkhare.homedropapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.PositioningManager;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;

public class Here extends Activity {

    private Map map = null;
    private PositioningManager posmanager;
    private MapFragment mapFragment = null;
    public GeoCoordinate userslocation;
    public String mylocation;
    public TextView locationshower;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(Here.this,"Welcome to Here Maps, Loading GPS shortly!",Toast.LENGTH_SHORT).show();
        initialize();
       // locationshower=findViewById(R.id.currentlocationviewer);
       // locationshower.append("Current location: "+userslocation);
    }

    private void initialize() {
        setContentView(R.layout.activity_here);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapfragment);

        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(OnEngineInitListener.Error error) {
                if (error == OnEngineInitListener.Error.NONE) {
                    map = mapFragment.getMap();
                    posmanager=PositioningManager.getInstance();
                    posmanager.start(PositioningManager.LocationMethod.GPS_NETWORK);
                    map.getPositionIndicator().setVisible(true);
                    map.setZoomLevel((map.getMaxZoomLevel()));
                    //userslocation= posmanager.getPosition().getCoordinate();
                } else {
                    System.out.println("ERROR: Cannot initialize Map Fragment");
                }
            }
        });
    }
}