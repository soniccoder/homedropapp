package com.example.harshkhare.homedropapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.harshkhare.homedropapp.R;
import com.example.harshkhare.homedropapp.UserPage;

public class Login extends AppCompatActivity {

    public EditText editText3, editText4;
    private Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText3 = findViewById(R.id.editText3);
        editText4 = findViewById(R.id.editText4);
        loginButton = findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                admin();
            //    user();

            }


        });

    }


    private void admin()
    {

        if (editText3.getText().toString().equals("Admin_Harsh") && editText4.getText().toString().equals("1234")) {
            Intent intent = new Intent(Login.this, com.example.harshkhare.homedropapp.AdminPage.class);
            startActivity(intent);
            // Toast.makeText(Login.this, "Please Enter Valid Admin Id", Toast.LENGTH_LONG).show();
        } else if(editText3.getText().toString().equals("User_Nitesh") && editText4.getText().toString().equals("1234"))
        {
            Intent intent = new Intent(Login.this, UserPage.class);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(Login.this, "Please Enter Valid User Id and Password", Toast.LENGTH_LONG).show();
        }

    }


}