package com.example.harshkhare.homedropapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class LoadingActivity extends AppCompatActivity {
    private int timer=2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        LogoLauncher logoLauncher=new LogoLauncher();
        logoLauncher.start();
    }
    private class LogoLauncher extends Thread{

        public void run(){

            try{
                sleep(1000*timer);
            }
            catch (InterruptedException e){
                e.printStackTrace();
            }
            Intent intent= new Intent(LoadingActivity.this,MainActivity.class);
            startActivity(intent);
            LoadingActivity.this.finish();
        }
}
}