package com.example.harshkhare.homedropapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.harshkhare.homedropapp.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;
import java.util.jar.Attributes;

public class EmployeeData extends AppCompatActivity {


    public static final String EmployeeName = "Employee Name";
    public static final String EmployeeId = "Employee ID";
    public static final String EmployeeUserId = "Employee UserId";
    public static final String EmployeePass = "Employee Password";
    public static final String EmployeeAddr = "Employee Address";
    public static final String EmployeeDesig = "Employee Designation";
    public static final String TAG = " Hello ";






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_data);

    }

    public void empdata(View view)
    {

        EditText Name = findViewById(R.id.name);
        EditText Empid = findViewById(R.id.empid);
        EditText Userid = findViewById(R.id.userid);
        EditText Pass = findViewById(R.id.pass);
        EditText Addr = findViewById(R.id.addr);
        EditText Desig = findViewById(R.id.desig);

        String Emp_name = Name.getText().toString();
        String Emp_id = Empid.getText().toString();
        String User_id= Userid.getText().toString();
        String Emp_pass = Pass.getText().toString();
        String Emp_Addr = Addr.getText().toString();
        String Emp_desig = Desig.getText().toString();

        DocumentReference Docref = FirebaseFirestore.getInstance().collection("UserData").document(User_id);

        if (Emp_name.isEmpty() || Emp_id.isEmpty() || User_id.isEmpty() || Emp_pass.isEmpty() || Emp_Addr.isEmpty() || Emp_desig.isEmpty() )
        {

            return; }

        Map<String, Object> datatosave = new HashMap<String, Object>();
        datatosave.put(EmployeeName, Emp_name);
        datatosave.put(EmployeeId, Emp_id);
        datatosave.put(EmployeeUserId, User_id);
        datatosave.put(EmployeePass, Emp_pass);
        datatosave.put(EmployeeAddr, Emp_Addr);
        datatosave.put(EmployeeDesig, Emp_desig);

        Toast.makeText(EmployeeData.this,"Your entry has been saved",Toast.LENGTH_SHORT).show();

        Docref.set(datatosave).addOnSuccessListener(new OnSuccessListener<Void>()
                                                    {
                                                        @Override
                                                        public void onSuccess(Void aVoid)
                                                        {
                                                            Log.d(TAG, "Document has been saved!");
                                                        }
                                                    }
        ).addOnFailureListener(new OnFailureListener()
                               {
                                   @Override
                                   public void onFailure(@NonNull Exception e)
                                   {
                                       Log.w(TAG, "Document was not saved!", e);
                                   }

                               }

        );

    }
}
