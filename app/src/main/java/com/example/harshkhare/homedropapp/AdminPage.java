package com.example.harshkhare.homedropapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.harshkhare.homedropapp.R;

public class AdminPage extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_page);

        Button button =  findViewById(R.id.button1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                employeedata();
            }
        });

        Button button1 = findViewById(R.id.button4);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openroster();
            }


        });


        Button button7 = findViewById(R.id.button2);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckStatusemp();
            }


        });

    }

    public void employeedata() {

        Intent intent = new Intent(AdminPage.this, EmployeeData.class);
        startActivity(intent);

    }

    private void openroster() {

        Intent intent = new Intent(this, com.example.harshkhare.homedropapp.Roster.class);

        startActivity(intent);

    }

    private void CheckStatusemp() {
        Intent intent = new Intent(this, com.example.harshkhare.homedropapp.CheckStatus.class);

        startActivity(intent);



    }


}
